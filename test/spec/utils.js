'use strict';

describe('utils', function () {

	beforeEach(module('lgUtils'));

	describe('List Operation', function () {

		var array;
		var list;

		beforeEach(inject(function (List) {
			array = [1,2,3];
			list = List;
		}));

		it('should remove one', function () {

			list.remove(array, function(item){
				return item === 3;
			});

		    expect(array).toEqual([1,2]);

		});

		it('should remove multi', function () {

			list.remove(array, function(item){
				return item === 3 || item === 2;
			});

		    expect(array).toEqual([1]);

		});

		it('should find one', function () {

			var hits = list.findOne(array, function(item){
				return item === 3;
			});

		    expect(hits).toEqual(3);

		});

		it('should find multi', function () {

			var hits = list.findMany(array, function(item){
				return item === 3 || item === 2;
			});

		    expect(hits).toEqual([2, 3]);

		});
	});
});
