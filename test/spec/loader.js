'use strict';

describe('Loader Service', function () {
	beforeEach(module('lgLoader'));

	var loader, mockBackend;

	beforeEach(inject(function ($httpBackend, ResourceLoader) {
		mockBackend = $httpBackend;
		loader = new ResourceLoader('mock');

	}));

	it('should fetch one', function () {
		var mockResponse = {title :'mock resource'};

		var holder;
		mockBackend.expectGET('/mock/1').respond(mockResponse);

		//var promise = loader.fetchOne('mock', 1);
		var promise = loader.fetchOne(1);

		promise.then(function(res){
			holder = res;
		});

		expect(holder).toBeUndefined();
		mockBackend.flush();

		expect(angular.equals(holder, mockResponse)).toBe(true);

	});

	it('should fetch all', function () {
		var mockResponse = [1,2,3];

		var holder;

		mockBackend.expectGET('/mock').respond(mockResponse);
		//var promise = loader.fetchAll('mock');
		var promise = loader.fetchAll('mock');

		promise.then(function(res){
			holder = res;
		});

		expect(holder).toBeUndefined();
		mockBackend.flush();

		expect(angular.equals(holder, mockResponse)).toBe(true);
	});

});

