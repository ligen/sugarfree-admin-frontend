'use strict';

angular.module('lgLoader', ['ngResource']) 
.factory('ResourceLoader', ['$q', '$resource', function($q, $resource){

	return function(name){

		var resource = $resource('/' + name +'/:id', {id:'@id'});

		return {

			save : function(obj){
				obj.$save();
			},

			fetchOne : function(id){
			
				var delay = $q.defer();
				resource.get({id:id}, function(data){
					console.log(data);
					delay.resolve(data);
				}, function(error){
					delay.reject(error + 'Unable to fetch banner' + id );
				});

				return delay.promise;
			},

			fetchAll : function(){
			
				if('member' === name){
					return [
						{id:1, name:'muchuan', region:'无糖圣地', email:'hj_kang@wesugarfree.com'},
						{id:2, name:'muchuan', region:'无糖圣地', email:'hj_kang@wesugarfree.com'},
						{id:3, name:'muchuan', region:'无糖圣地', email:'hj_kang@wesugarfree.com'}
					];
				}

				/*
				if('banner' === name){
				
					return [
						{id:1, url:'example.org', img:'test.org'},
						{id:2, url:'example.org', img:'test.org'},
						{id:3, url:'example.org', img:'test.org'}
					];
				}

				 */
				var delay = $q.defer();
				resource.query(function(data){
					delay.resolve(data);
				}, function(error){
					delay.reject('Unable to fetch banners' );
					console.log('Unable to fetch banners');
					console.log(error);
				});

				return delay.promise;
			}
		
		};
	
	};

}]);

