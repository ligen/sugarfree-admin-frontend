'use strict';

angular.module('lgUtils', [])
.factory('List', function(){
    return {

        remove : function(array, cond){
            for(var i = array.length -1; i >= 0; i--){
                if(cond(array[i]) === true){
                    array.splice(i, 1);
                }
            }
        },

        findMany : function(array, cond){
            var hits = [];
            for(var i = 0; i < array.length; i++){
                if(cond(array[i]) === true){
                    hits.push(array[i]);
                }
            }
            return hits;
        },

        findOne : function(array, cond){
            for(var i = 0; i < array.length; i++){
                if(cond(array[i]) === true){
                    return array[i];
                }
            }
        }
    };

});

