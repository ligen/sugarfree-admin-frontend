'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp:constant
 * @description
 * # constant
 * constant of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .constant('appConstant',{
  	bannerType:[
  	     {value:1,name:'单次课'},
                  {value:2,name:'课程包'},
                  {value:4,name:'课程包列表'}
  	]
  });
