'use strict';

/**
 * @ngdoc overview
 * @name sugarfreeAdminFrontendApp
 * @description
 * # sugarfreeAdminFrontendApp
 *
 * Main module of the application.
 */

angular
.module('admin', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'lgTable',
    //'lgLoader',
])
.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
    })
    .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
    })
    .when('/:module', {
        templateUrl: 'views/table.html',
        controller: 'TableCtrl',
        controllerAs: 'tableCtrl',
        resolve:{

			/*
			Resource : function($routeParams, ResourceLoader) {
				return function(){
					return new ResourceLoader($routeParams.module);
				};
			},

			 */
			resource: function($routeParams, $resource) {
				return function(){
					return $resource('/admin/' + $routeParams.module + '/:id', {id:'@id'});
				};
			},

			TableParams : function($routeParams, Columns){ 

				return {

					columns : function(){ return Columns[$routeParams.module];},

					params : function(){return {}; },

					settings : function(){ return { counts:[] }; }

				};
            }

        }
    })
    .otherwise({
        redirectTo: '/'
    });
})
.factory('Columns', function(){
	return {
		region : [
		
			{ width:'15', field:'shengShi', title:'省/市', dataType:'text' },
			{ width:'15', field:'shiQuXian', title:'市/区/县', dataType:'text' },
			{ width:'30', field:'shangQuan', title:'商圈', dataType:'text' },
			{ width:'30', field:'weight', title:'优先级', dataType:'number' },
			{ width:'10', field:'action', title:'', dataType:'command' }
		],
		banner : [
			{ width:'15', field:'title', title:'标题', dataType:'text' },
			{ width:'15', field:'description', title:'描述', dataType:'text' },
			{ width:'15', field:'shareurl', title:'分享链接', dataType:'text' },
			{ width:'15', field:'shareimage', title:'分享图片', dataType:'text' },
			{ width:'15', field:'url', title:'链接', dataType:'text' },
			{ width:'15', field:'image', title:'图片', dataType:'text' },
			{ width:'15', field:'type', title:'类型', dataType:'select' },
			{ width:'10', field:'action', title:'', dataType:'command' }
		],
		member : [
			{ width:'10', field:'name', title:'名字', dataType:'text' },
			{ width:'40', field:'region', title:'负责区域', dataType:'text' },
			{ width:'40', field:'email', title:'邮箱', dataType:'text' },
			{ width:'10', field:'action', title:'', dataType:'command' }
		] 
	};
});
