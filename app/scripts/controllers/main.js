'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .controller('MainCtrl', ['$scope', function ($scope) {
  }]);
