'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .controller('CenterCtrl', ['$scope','$window','$location', function ($scope,$window,$location) {
	// $scope.setActive = function(activeUrl){
	// 	$scope.creentActive = activeUrl;
	// 	$window.location.href = "#"+activeUrl;
	// };

	$scope.isRoute = function (path) {
		// console.info(path);
    		return $location.path() === path;
  	};

	function initData(){
		
	}

	initData();
  }]);
