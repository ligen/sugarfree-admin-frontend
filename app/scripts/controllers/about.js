'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
