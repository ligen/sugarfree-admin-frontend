'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp.controller:BannerModalCtrl
 * @description
 * # BannerModalCtrl
 * Controller of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .controller('BannerModalCtrl',['$scope','$resource','$uibModalInstance','items',function ($scope,$resource,$uibModalInstance,items) {
            var Banner = $resource('/admin/banner/:id',{id:'@id'});

   	      $scope.closeModel = function(){
                $uibModalInstance.dismiss('cancel');
             }


              $scope.addBanner = function(){
                console.info('$scope.banner',$scope.banner);
                Banner.save($scope.banner,function(rsps){
                  $uibModalInstance.close('add-success!');
                  console.info(rsps);
                },function(rsps){
                  $uibModalInstance.dismiss('cancel');
                  console.info(rsps);
                });
              };

             function initData(){
                console.info('items',items);
                if(items){
                  $scope.banner = items;
                }
                $scope.isshows=[
                  {value:'1',name:'是'},
                  {value:'2',name:'否'}
                ];


                $scope.types=[
                  {value:1,name:'单次课'},
                  {value:2,name:'课程包'},
                  {value:4,name:'课程包列表'}
                ]; 
             }
             initData();
  }]);
