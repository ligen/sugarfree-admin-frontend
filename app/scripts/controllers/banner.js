'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp.controller:BannerCtrl
 * @description
 * # BannerCtrl
 * Controller of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .controller('BannerCtrl',['$scope','$resource','$http','$uibModal','NgTableParams',function ($scope,$resource,$http,$uibModal,NgTableParams) {
   	var Banner = $resource('/admin/banner/:id',{id:'@id'});

      $scope.newBanner = function(banner){
        //console.info('bannerId',bannerId);
        var modalInstance = $uibModal.open({
          animation: true,
          keyboard:true,
          backdrop:'static',
          templateUrl: 'views/banner-add.html',
          controller:'BannerModalCtrl',
          size: 'lg',
          resolve: {
            items: function () {
              return angular.copy(banner);
            }
          }
        });

       modalInstance.result.then(function (selectedItem) {
          $scope.tableParams.reload();
       }, function (reason) {
              console.info(reason);
       });
      };

      function initData(){
        $scope.tableParams = new NgTableParams({}, {counts: [],getData:Banner.query});
        $scope.types=[
                  {value:1,name:'单次课'},
                  {value:2,name:'课程包'},
                  {value:4,name:'课程包列表'}
        ]; 
      }
      initData();
  }]);
