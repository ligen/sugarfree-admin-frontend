'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp.utils:directives
 * @description
 * # directives
 * directives of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .directive('qiniuUpload',['$timeout','$log',function($timeout,$log){
      var log = false;
      return {
        restrict:'AE',
        replace:true,
        scope:{
          files:'='
        },
        link:function(scope,iElement,iAttrs){
           if(log){
              $log.info("scope",scope);
              $log.info("iElement",iElement);
              $log.info("iAttrs",iAttrs);
           }
           var pickfiles =  iAttrs.id;
           var uptokenUrl = iAttrs.uptokenUrl;
           var uptoken = iAttrs.uptoken;
           var uploader;
             if(!uploader){
                var qiniu = new QiniuJsSDK();         
                uploader = qiniu.uploader({
                runtimes: 'html5,html4',
                browse_button: pickfiles,
                container: "container",
                //drop_element: 'container',
                max_file_size: '10mb',
                // flash_swf_url: 'js/plupload/Moxie.swf',
                dragdrop: false,
                chunk_size: '4mb',
                uptoken_url: uptokenUrl,
                uptoken:uptoken,
                domain: 'http://qiniu-plupload.qiniudn.com/',
                save_key: true,
                auto_start: true,
                init: {
                  'FilesAdded': function(up, files) {
                    console.log("FilesAdded");
                  },
                  'BeforeUpload': function(up, file) {
                    console.log("BeforeUpload");
                  },
                  'UploadProgress': function(up, file) {
                    console.log('UploadProgress',up,file);
                  },
                  'UploadComplete': function() {
                    console.log('UploadComplete');
                  },
                  'FileUploaded': function(up, file, info) {
                    console.error("up",up);
                    console.error("file",file);
                    console.error("info",info);
                    scope.$apply(function(){
                      var _info = JSON.parse(info);
                      _info.id = file.id;
                      scope.files.push(angular.copy(_info));
                    });
                    console.info("scope.files",scope.files);
                  },
                  'Error': function(up, err, errTip) {
                    console.log('Error');
                    console.info('err',err,errTip);
                    if(err.code===-600){
                      alert('上传图片出错！请上传小于10M的图片');
                    }else{
                      alert('上传图片出错！');
                    }
                  },
                  'Key': function(up, file) {
                          var key = key;
                          // do something with key here
                          return key;
                      }
                }
              });
             }
        }
      };
  }]);





  
