'use strict';

/**
 * @ngdoc function
 * @name sugarfreeAdminFrontendApp.utils:filter
 * @description
 * # filter
 * filter of the sugarfreeAdminFrontendApp
 */
angular.module('admin')
  .filter('bannerTypeFilter',['$filter','appConstant',function($filter,appConstant) {
    return function(input){
	var found = $filter('filter')(appConstant.bannerType, { value: input},true);
      return found[0].name;
    }
  }])
  .filter('lineFilter',[function() {
    return function(input){
      return input.replace(/\n/g,'<br/>');
    }
  }]);



  
