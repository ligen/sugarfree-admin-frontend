'use strict';

/**
 * @ngdoc overview
 * @name sugarfreeAdminFrontendApp
 * @description
 * # sugarfreeAdminFrontendApp
 *
 * Main module of the application.
 */

angular
.module('admin', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    //'lgTable',
    'ngTable',
    'ui.bootstrap'
    //'lgLoader',
])
.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
    })
    .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
    })
    .when('/banner',{
        templateUrl: 'views/banner.html',
        controller: 'BannerCtrl',
        controllerAs: 'banner'	
    })
    .when('/setting',{
        templateUrl: 'views/banner.html',
        controller: 'BannerCtrl',
        controllerAs: 'banner'  
    })
    .otherwise({
        redirectTo: '/'
    });
});
